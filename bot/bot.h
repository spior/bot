
#include "targetver.h"

#include <stdio.h>
#include <stdlib.h>
#include <tchar.h>
//#include "../client/client.h"
#include <winsock2.h>
#include <windows.h>

// Under-complicating some base functions
#define Com_Memcpy memcpy
#define Com_Memset memset
#define Com_Memcpy memcpy
#define Z_Malloc(size, x, y) calloc(1, size)
#define S_Malloc malloc
#define Z_Free free

// Console-related stuff
extern int console_width;
extern int console_height;

void CLR_Printf(const char *buffer, int (*prnt)(const char *, ...), int attrib);
void CON_Input(void);
BOOL CON_SetSize(const int width, const int height, const int scroll_mass);
void Con_CenterPrint(int color, const char *fmt);