// bot.cpp : Defines the entry point for the console application.
//

#include "bot.h"
#include "winsock.h"
#include "windows.h"
#include "../qcommon/q_shared.h"
#include "../qcommon/qcommon.h"

#define UDP SOCK_DGRAM
#define TCP SOCK_STREAM

#define SEND(str) send(socks_socket, str, strlen(str), 0);

void CopyShortSwap(void *dest, void *src)
{
    byte *to = (byte*)dest, *from = (byte*)src;

    to[0] = from[1];
    to[1] = from[0];
}

void CopyLongSwap(void *dest, void *src)
{
    byte *to = (byte*)dest, *from = (byte*)src;

    to[0] = from[3];
    to[1] = from[2];
    to[2] = from[1];
    to[3] = from[0];
}

u_long LookupAddress(const char* pcHost, byte *ip)
{
    u_long nRemoteAddr = inet_addr(pcHost);
    if (nRemoteAddr == INADDR_NONE) {
        // pcHost isn't a dotted IP, so resolve it through DNS
        struct hostent* pHE = gethostbyname(pcHost);
        if (pHE == 0) {
            return INADDR_NONE;
        }
        nRemoteAddr = *((u_long*)pHE->h_addr_list[0]);
    }

	// Holy shit that compiled?!
	ip[0] = ((IN_ADDR*)&nRemoteAddr)->S_un.S_un_b.s_b1;
	ip[1] = ((IN_ADDR*)&nRemoteAddr)->S_un.S_un_b.s_b2;
	ip[2] = ((IN_ADDR*)&nRemoteAddr)->S_un.S_un_b.s_b3;
	ip[3] = ((IN_ADDR*)&nRemoteAddr)->S_un.S_un_b.s_b4;
	//sscanf(inet_ntoa(temp), "%c.%c.%c.%c", ip[0], ip[1], ip[2], ip[3]);
    return nRemoteAddr;
}

//netchan_t chan;
//SOCKET socks_socket;
//int qport;

#define MAX_EDIT_LINE 150
HANDLE qconsole_hout = GetStdHandle(STD_OUTPUT_HANDLE);
HANDLE qconsole_hin = GetStdHandle(STD_INPUT_HANDLE);
char qconsole_line[MAX_EDIT_LINE];
int qconsole_linelen = 0;
WORD qconsole_attrib = 0;
int console_width = 0;
int console_height = 0;

int colors[] = {
	(FOREGROUND_INTENSITY),										// JKA_BLACK
	(FOREGROUND_INTENSITY|FOREGROUND_RED), 						// JKA_RED
	(FOREGROUND_INTENSITY|FOREGROUND_GREEN), 					// JKA_GREEN 
	(FOREGROUND_INTENSITY|FOREGROUND_RED|FOREGROUND_GREEN),		// JKA_YELLOW
	(FOREGROUND_INTENSITY|FOREGROUND_BLUE),						// JKA_BLUE 
	(FOREGROUND_INTENSITY|FOREGROUND_GREEN|FOREGROUND_BLUE), 	// JKA_CYAN 
	(FOREGROUND_INTENSITY|FOREGROUND_RED|FOREGROUND_BLUE), 		// JKA_MAGNETA 
	(FOREGROUND_RED|FOREGROUND_BLUE|FOREGROUND_GREEN)			// JKA_WHITE
};

static void CON_Show( void )
{
	CONSOLE_SCREEN_BUFFER_INFO buf;
	COORD size = { console_width-1, 1 };
	COORD coord = { 0, 0 };
	SMALL_RECT write = { 0, 0, 0, 0 };
	CHAR_INFO line[ MAX_EDIT_LINE ];
	int i, a;
	int qconsole_linelen_buf = -1; // length of the input without the ^#

	GetConsoleScreenBufferInfo( qconsole_hout, &buf );

	//if(buf.dwCursorPosition.X != 0)
	//	return;

	write.Left = 0;
	write.Top = buf.dwCursorPosition.Y; 
	write.Bottom = buf.dwCursorPosition.Y; 
	write.Right = MAX_EDIT_LINE;

	// set color to white
	qconsole_attrib = RGB2C(ColorForIndex(7));
	// added the 'a' variable in here because of reasons
	for( i=0, a=0; i<MAX_EDIT_LINE; i++, a++ )
	{
		if(i > 0 && Q_IsColorString(qconsole_line+(i-1)))
		{
			qconsole_attrib = RGB2C(ColorForIndex(qconsole_line[i]-'0'));

			a-=2;
			continue;
		}

		if( i < qconsole_linelen )
			line[a].Char.AsciiChar = qconsole_line[i];
		else {
			line[a].Char.AsciiChar = ' ';
			if(qconsole_linelen_buf == -1)
				qconsole_linelen_buf = a;
		}

		line[a].Attributes = qconsole_attrib;
	}

	if(qconsole_linelen_buf > buf.srWindow.Right)
	{
		WriteConsoleOutput( qconsole_hout, 
			line + (qconsole_linelen_buf - buf.srWindow.Right ),
			size, coord, &write );
	} else {
		WriteConsoleOutput( qconsole_hout, 
			line, 
			size, coord, &write );
	}
	COORD pos = { qconsole_linelen_buf, buf.dwCursorPosition.Y };
	SetConsoleCursorPosition(qconsole_hout, pos);
}

void CON_Input(void)
{
	int newlinepos = -1;
	DWORD len = 0;
	if(!GetNumberOfConsoleInputEvents( qconsole_hin, &len ))
		return;

	if(len < 1)
		return;

	INPUT_RECORD buf[MAX_EDIT_LINE];
	DWORD read = 0;
	if(!ReadConsoleInput(qconsole_hin, buf, len, &read))
		return;

	if(read > MAX_EDIT_LINE)
		read = MAX_EDIT_LINE;

	for( int i = 0; i < read; i++ )
	{
		if( buf[i].EventType != KEY_EVENT )
			continue;
		if( !buf[i].Event.KeyEvent.bKeyDown ) 
			continue;

		WORD key = buf[i].Event.KeyEvent.wVirtualKeyCode;

		if( key == VK_RETURN )
		{
			newlinepos = i;
			break;
		}

		if(		key == VK_UP
			||	key == VK_DOWN
			||	key == VK_TAB )
			break;

		if( key == VK_BACK )
		{
			int pos = ( qconsole_linelen > 0 ) ?
				qconsole_linelen - 1 : 0; 

			qconsole_line[pos] = 0;
			qconsole_linelen = pos;
			break;
		}

		if( qconsole_linelen < sizeof( qconsole_line ) - 1 )
		{
			char c = buf[i].Event.KeyEvent.uChar.AsciiChar;
			if( c )
			{
				qconsole_line[qconsole_linelen++] = c;
				qconsole_line[qconsole_linelen] = 0; 
			}
		}
	}

	if( newlinepos < 0) {
		CON_Show();
		return;
	}

	if( !qconsole_linelen )
	{
		CON_Show();
		Com_Printf( "\n" );
		return;
	}

	Com_Printf( "%s\n", qconsole_line );
	Cbuf_AddText(qconsole_line);

	qconsole_linelen = 0;
	CON_Show();
}

BOOL CON_SetSize(const int width, const int height, const int scroll_mass)
{
	BOOL ret;
	COORD coord = { width, height*scroll_mass };
	ret = SetConsoleScreenBufferSize(qconsole_hout, coord);
	if(!ret)
		return ret;
	SMALL_RECT rect = { 0, 0, width-1, height-1 };
	ret = SetConsoleWindowInfo(qconsole_hout, 1, &rect);
	console_width = width;
	console_height = height;
	return ret;
}

void CLR_Printf(const char *buffer, int (*prnt)(const char *, ...), int attrib)
{
	if(qconsole_linelen && !attrib)
	{
		CONSOLE_SCREEN_BUFFER_INFO cur;
		GetConsoleScreenBufferInfo(qconsole_hout, &cur);
		cur.dwCursorPosition.X = 0;
		// Clear this line
		COORD temp = { 0, cur.dwCursorPosition.Y };
		SetConsoleCursorPosition(qconsole_hout, temp);
		for(int i=0; i<console_width; i++) printf(" ");
		SetConsoleCursorPosition(qconsole_hout, cur.dwCursorPosition);
	}

	if(attrib)
		SetConsoleTextAttribute(qconsole_hout, attrib);

	char *buf = (char*)malloc(strlen(buffer)+2);
	char *p;
	memset(buf, 0, strlen(buffer)+2);
	strcpy(buf, buffer);
	for(p = buf; *p; p++)
	{
		char *f; 
		size_t len;
		char *t;
		if(p != buf && Q_IsColorString(p-1))
		{
			int atr = 
				attrib & BACKGROUND_INTENSITY ? 
					p[0] == '0' ? 
					RGB2C(ColorForIndex(ColorIndex(p[0])))+attrib-FOREGROUND_INTENSITY
					: RGB2C(ColorForIndex(ColorIndex(p[0])))+attrib
				: RGB2C(ColorForIndex(ColorIndex(p[0])))+attrib;
			SetConsoleTextAttribute(qconsole_hout, atr);
			p++;
		}
		else if(p != buf) // so it doesn't put a ^ at the beginning of every message
			fputc(Q_COLOR_ESCAPE, stderr);
		f = strchr(p, Q_COLOR_ESCAPE);
		len = f ? f-p : strlen(p);
		t = (char*)malloc(len+1);
		memset(t, 0, len+1);
		strncpy(t, p, len);
		fputs(t, stderr);
		free(t);
		p+=len;
	}
	SetConsoleTextAttribute(qconsole_hout, RGB2C(ColorForIndex(7)));
	free(buf);

	//CON_Show();
}

// And the award for the uglies function ever goes to...
void Con_CenterPrint(int color, const char *text)
{
	char final_text[32000] = { 0 }; // cbf calculating exact length

	int nocolor = 0;
	int l = strlen(text);
	for(int i=0; i<l; i++) if(Q_IsColorString(text+i)) i++; else nocolor++;

	sprintf(final_text, "%c%d", Q_COLOR_ESCAPE, color);
	
	for(int i=0; i<console_width; i++) strcat(final_text, "*");
	int len = 0, colors = 0;
	for(int k=0, c=0; c<nocolor; k+=len+colors, c+=len)
	{
		const char *newline = strchr(text+k, '\n'); newline = newline ? newline : strchr(text+k, '\r');
		if(newline)
		{
			int t = 0;
			len = (newline-(text+k));
			for(int i=k; i<k+len; i++)
				if(Q_IsColorString(text+i))
					int SOMETEMPVARIABLEFUCK = (i++, t++);
			len-=t*2;
		}
		else
			len = nocolor-c > console_width ? console_width : nocolor-c;
		colors=0;
		for(int i=k, t=0; i<l; i++)
		{
			if(t==console_width || (newline && t==len)) break;
			if(Q_IsColorString(text+i))
			{
				colors+=2;
				i++;
			} else t++;
		}
		for(int i=0; i<(console_width-len)/2; i++) strcat(final_text, " ");
		strncat(final_text, text+k, len+colors);
		for(int i=0; i<(console_width-len)/2; i++) strcat(final_text, " ");
		if(console_width%2 && !(len%2)) strcat(final_text, " "); // in case it was uneven
		if(newline) int USELESSVAR = (k++, c++); // Yeah ugh this is just so we skip the Cr/Lf
	}
	final_text[strlen(final_text)] = Q_COLOR_ESCAPE;
	final_text[strlen(final_text)] = color+'0';
	for(int i=0; i<console_width; i++)  strcat(final_text, "*");

	CLR_Printf(final_text, printf, BACKGROUND_INTENSITY);
}

//int _tmain(int argc, char* argv[])
//{
//	// Set the console size to fit jamp's one exactly
//	CON_SetSize(97, 25, 10);
//	//CreateThread(NULL, NULL, Input, NULL/*this is where args go*/, NULL, NULL);
//
//	if(argc < 2)
//	{
//		printf("USAGE: %s <host>:<port>[29070]\n", argv[0]);
//		return 0;
//	}
//
//	char host[512] = { 0 };
//	strcpy(host, argv[1]);
//
//	int port;
//	if(strchr(argv[1], ':'))
//	{
//		char port_s[1024] = { 0 };
//		strccpyfrom(port_s, argv[1], ":");
//		port = atoi(port_s);
//		host[strchr(host, ':')-host] = 0;
//	}
//	else
//		port = 29070;
//
//	WSADATA wsaData;
//
//	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0) {
//		printf("WSAStartup failed.\n");
//		return 0;
//	}
//
//	//netadr_t from;
//	//memset(&from, 0, sizeof(from));
//	u_long nRemoteAddress = LookupAddress(host, (byte*)clc.serverAddress.ip);
//	clc.serverAddress.type = NA_IP;
//	clc.serverAddress.port = port;
//
//
//    if (nRemoteAddress == INADDR_NONE) {
//        //printf("%s\n", WSAGetLastErrorMessage("lookup address"));
//        return 0;
//    }
//
//	socks_socket = socket(AF_INET, UDP, IPPROTO_UDP);
//
//	SOCKADDR_IN SockAddr_loc;
//    SockAddr_loc.sin_family = AF_INET;
//    SockAddr_loc.sin_port = htons(0);
//    SockAddr_loc.sin_addr.s_addr = 0;
//
//    bind(socks_socket, (SOCKADDR*)(&SockAddr_loc), sizeof(SockAddr_loc));
//
//	SOCKADDR_IN SockAddr;
//	SockAddr.sin_port=htons(port);
//	SockAddr.sin_family=AF_INET;
//	SockAddr.sin_addr.s_addr = nRemoteAddress;
//
//	if(connect(socks_socket,(SOCKADDR*)(&SockAddr),sizeof(SockAddr)) == INVALID_SOCKET){
//		closesocket(socks_socket);
//		WSACleanup();
//		return 0;
//	} else {
//		u_long lng;
//		ioctlsocket(socks_socket, FIONBIO, &lng);
//		srand(time(NULL));
//		qport = rand();
//		cls.state = CA_CONNECTING;
//		// Com_Init kinda
//		Com_Init("");
//		/*Cvar_Init ();
//		Cbuf_Init();
//		Cmd_Init ();
//		FS_InitFilesystem ();
//		atexit(disconnect);*/
//	}
//	/*int bytes;
//	byte bufData[MAX_MSGLEN+1] = { 0 };
//	msg_t msg;
//	MSG_Init(&msg, bufData, sizeof ( bufData ) );
//
//	while (bytes = recv(socks_socket, (char*)msg.data, msg.maxsize, 0))
//	{
//        msg.readcount = 0;
//        msg.cursize = bytes;
//		CL_SendCmd();
//		memset(&msg, 0, sizeof(msg));
//		MSG_Init(&msg, bufData, sizeof ( bufData ) );
//
//	}*/
//	while(1)
//	{
//		// Com_Frame...and stuff
//		static int lastTime = 0;
//		com_frameTime = timeGetTime();
//		if ( lastTime > com_frameTime )
//			lastTime = com_frameTime;
//		int msec = com_frameTime - lastTime;
//		if(msec < 1) msec = 1;
//		lastTime = com_frameTime;
//		msg_t msg;
//		memset(&msg, 0, sizeof(msg));
//		byte bufData[MAX_MSGLEN+1] = { 0 };
//		MSG_Init(&msg, bufData, sizeof(bufData));
//		int bytes = recv(socks_socket, (char*)msg.data, msg.maxsize, 0);
//		if(bytes == -1 && GetLastError() != WSAEWOULDBLOCK) {}
//		else if(bytes != -1)
//		{
//			msg.cursize = bytes;
//			CL_PacketEvent(clc.serverAddress, &msg);
//		}
//		Cbuf_Execute();
//		CL_Frame(msec);
//		Sleep(10); // Take a deep breath, CPU
//	}
//	return 0;
//}
//
